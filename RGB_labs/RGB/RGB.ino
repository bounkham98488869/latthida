// C++ code
//
int ledPins[] = {11, 10, 9}; // Red, Green, Blue
int ledLength = sizeof(ledPins) / sizeof(ledPins[0]);

void setup()
{
  Serial.begin(9600);
      
  for (int i = 0; i < ledLength; i++){
  	pinMode(ledPins[i], OUTPUT);
  }

}

void loop()
{
  Serial.print("Mode 1 ");
  Serial.println("RGB");
  
  for(int pin : ledPins){
  	digitalWrite(pin, HIGH);
    delay(1000);
    
  	digitalWrite(pin, LOW);
    delay(1000);
  }
  redGreen();
  redBlue();
  greenBlue();
  white();
}

//Fuctions custom colors
void redGreen(){
  Serial.print("Mode 2 ");
  Serial.println("R+G");
  digitalWrite(ledPins[0], HIGH);
  digitalWrite(ledPins[1], HIGH);
  delay(1000);

  digitalWrite(ledPins[0], LOW);
  digitalWrite(ledPins[1], LOW);
  delay(1000);
}

void redBlue(){
  Serial.print("Mode 3 ");
  Serial.println("R+B");
  digitalWrite(ledPins[0], HIGH);
  digitalWrite(ledPins[2], HIGH);
  delay(1000);
    
  digitalWrite(ledPins[0], LOW);
  digitalWrite(ledPins[2], LOW);
  delay(1000);
}

void greenBlue(){
  Serial.print("Mode 4 ");
  Serial.println("G+B");
  digitalWrite(ledPins[1], HIGH);
  digitalWrite(ledPins[2], HIGH);
  delay(1000);
    
  digitalWrite(ledPins[1], LOW);
  digitalWrite(ledPins[2], LOW);
  delay(1000);
}

void white(){
  Serial.print("Mode 5 ");
  Serial.println("R+G+B");
  digitalWrite(ledPins[0], HIGH);
  digitalWrite(ledPins[1], HIGH);
  digitalWrite(ledPins[2], HIGH);
  delay(1000);
    
  digitalWrite(ledPins[0], LOW);
  digitalWrite(ledPins[1], LOW);
  digitalWrite(ledPins[2], LOW);
  delay(1000);
}